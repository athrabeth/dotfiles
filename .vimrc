map <C-b> :NERDTreeFind<CR>

set clipboard=unnamed
set shiftwidth=2
set tabstop=2
set smartindent
set expandtab
set wildmenu
set backspace=2
set ruler
set number
set undofile
set statusline=
syntax on

filetype plugin indent on
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

